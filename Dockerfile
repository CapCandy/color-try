FROM openjdk:latest
WORKDIR /app
MAINTAINER d.com
COPY ./target/try-color-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/app/try-color-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080

