package it.reply.trycolor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Color {

    @GetMapping("/")
    public String getColor() {
        switch ((int)((Math.random()*10)%3))
        {
            case 0:
                return "blue";
            case 1:
                return "red";
            case 2:
                return "yellow";
        }
        return "WTF";
    }
}
